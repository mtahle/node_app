FROM node:latest
RUN mkdir -p /opt/node_app
COPY ./node_passport_login/ /opt/node_app
WORKDIR /opt/node_app
RUN npm install
EXPOSE 5000
CMD npm run dev
